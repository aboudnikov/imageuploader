<?php
// Generating the navigation menu
if (isset($_SESSION['email'])) {?>
<div id="menubar">
    <ul id="menu">
        <li><a href="index.php">Home</a></li>
        <li><a href="uploadimages.php">Upload Images</a></li>
        <li><a href="showimages.php">My Images</a></li>
        <li><a href="viewprofile.php">View Profile</a></li>
        <li><a href="editprofile.php">Edit Profile</a></li>
        <li><a href="logout.php">Log Out</a></li>
    </ul>
</div>

<?php }
else { ?>
<div id="menubar">
    <ul id="menu">
        <li><a href="index.php">Home</a></li>
        <li><a href="uploadimages.php">Upload Images</a></li>
        <li><a href="login.php">Log In</a></li>
        <li><a href="signup.php">Sign Up</a></li>
    </ul>
</div>

<?php } ?>
