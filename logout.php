<?php
session_start();
if (isset($_SESSION['user_id']))
{
    // Clearing the $_SESSION array
    $_SESSION = array();

    // Deleting cookies
    if (isset($_COOKIE[session_name()]))
    {
        setcookie(session_name(), '', time() - 3600);
    }

    //Destroying the session
    session_destroy();
}

setcookie('user_id', '', time() - 3600);
setcookie('email', '', time() - 3600);

// Redirecting to the home page
$home_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/index.php';
header('Location: ' . $home_url);
?>
