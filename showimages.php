<?php
require_once('session.php');
require_once('connectioninfo.php');
require_once('templates/header.php');
require_once('dbconnect.php');

function sort_results ($sort)
{   //sorts results by image name, image type and upload date
    $query = "SELECT * FROM images where user_id = '" . $_SESSION['user_id'] . "'";
    switch ($sort)
    {

        case 1:
            $query .= " ORDER BY image_name";
            break;

        case 2:
            $query .= " ORDER BY image_name DESC";
            break;

        case 3:
            $query .= " ORDER BY image_type";
            break;

        case 4:
            $query .= " ORDER BY image_type DESC";
            break;

        case 5:
            $query .= " ORDER BY upload_date";
            break;

        case 6:
            $query .= " ORDER BY upload_date DESC";
            break;
        default:
    }

    return $query;
}


function generate_sort_links($sort)
{   //generates links to sort the results
    $sort_links = '';

    switch ($sort)
    {
        case 1:
            $sort_links .= '<td><a href = "' . $_SERVER['PHP_SELF'] . '?sort=2">Image Name </a></td>';
            $sort_links .= '<td><a href = "' . $_SERVER['PHP_SELF'] . '?sort=3">Image Type </a></td>';
            $sort_links .= '<td><a href = "' . $_SERVER['PHP_SELF'] . '?sort=5">Date Uploaded </a></td>';
            break;
        case 3:
            $sort_links .= '<td><a href = "' . $_SERVER['PHP_SELF'] . '?sort=1">Image Name </a></td>';
            $sort_links .= '<td><a href = "' . $_SERVER['PHP_SELF'] . '?sort=4">Image Type </a></td>';
            $sort_links .= '<td><a href = "' . $_SERVER['PHP_SELF'] . '?sort=5">Date Uploaded </a></td>';
            break;
        case 5:
            $sort_links .= '<td><a href = "' . $_SERVER['PHP_SELF'] . '?sort=1">Image Name </a></td>';
            $sort_links .= '<td><a href = "' . $_SERVER['PHP_SELF'] . '?sort=3">Image Type </a></td>';
            $sort_links .= '<td><a href = "' . $_SERVER['PHP_SELF'] . '?sort=6">Date Uploaded </a></td>';
            break;
        default:
            $sort_links .= '<td><a href = "' . $_SERVER['PHP_SELF'] . '?sort=1">Image Name </a></td>';
            $sort_links .= '<td><a href = "' . $_SERVER['PHP_SELF'] . '?sort=3">Image Type </a></td>';
            $sort_links .= '<td><a href = "' . $_SERVER['PHP_SELF'] . '?sort=5">Date Uploaded </a></td>';
    }

    return $sort_links;
}

function generate_page_links($sort, $cur_page, $num_pages)
{   //generates page links if there are more then 9 results on a page
    $page_links = '';

    // If this page is not the first page, generates the previous link
    if ($cur_page > 1)
    {
        $page_links .= '<a href="' . $_SERVER['PHP_SELF'] . '?sort=' . $sort . '&page=' . ($cur_page - 1) . '"><-</a> ';
    }
    else
    {
        $page_links .= '<- ';
    }

    // Generates page numbers links
    for ($i = 1; $i <= $num_pages; $i++)
    {
        if ($cur_page == $i)
        {
            $page_links .= ' ' . $i;
        }
        else
        {
            $page_links .= ' <a href="' . $_SERVER['PHP_SELF'] . '?sort=' . $sort . '&page=' . $i . '"> ' . $i . '</a>';
        }
    }

    // Generate the "next" link
    if ($cur_page < $num_pages)
    {
        $page_links .= ' <a href="' . $_SERVER['PHP_SELF'] . '?sort=' . $sort . '&page=' . ($cur_page + 1) . '">-></a>';
    }
    else
    {
        $page_links .= ' ->';
    }

    return $page_links;
}

$sort = $_GET['sort'];
$cur_page = isset($_GET['page']) ? $_GET['page'] : 1;
$results_per_page = 9;
$skip = (($cur_page - 1) * $results_per_page);
$query = sort_results($sort);
$data = mysqli_query($dbc, $query);
$total_results = mysqli_num_rows($data);
$num_pages = ceil($total_results / $results_per_page);

// Quering for a subset of results
$query =  $query . " LIMIT $skip, $results_per_page";
$data = mysqli_query($dbc, $query);
$total = mysqli_num_rows($data);
require_once('menu.php');
if (mysqli_num_rows($data) != 0)
{
?>

<div id="site_content">
    <p>Sort by: <?php echo generate_sort_links($sort);?></p> <br />

    <?php
    for ($i = 0; $i < $total; $i++)
    {
        $row = mysqli_fetch_assoc($data);
        $path = UPLOADPATH . $_SESSION['user_id'] . '/' . $row['image_name'];
        $class = (intval($i%3) == 0)?'list_image_left':((intval($i%3) == 1)?'list_image_center':'list_image_right');?>
        <div class="<?php echo $class ?>">
        <a href='imageview.php?id=<?php echo $row['image_id']?>'><img class="list_image" src="<?php echo $path ?>"></a>
        <a href ="deleteimage.php?image_id=<?php echo $row['image_id']?>&amp;image_name=<?php echo $row['image_name']?>&amp;image_type=<?php echo $row['image_type']?>&amp;upload_date=<?php echo $row['upload_date']?>&amp;user_id=<?php echo $_SESSION['user_id']?>">Delete</a>
        <br />
        </div>
    <?php
    }
    ?>
    <div style="clear:both" align = "center">
        <?php
        if ($num_pages > 1)
        {
            echo generate_page_links($sort, $cur_page, $num_pages);
        }
        ?>
</div>
<?php
    mysqli_close($dbc);
}
else
{
    echo "You have no images. You can upload them <a href='uploadimages.php'> here</a>";
}
?>
</div>

<?php
    require_once('templates/footer.php');
?>