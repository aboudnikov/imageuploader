<?php
require_once('session.php');
require_once('connectioninfo.php');
require_once('dbconnect.php');
require_once('templates/header.php');
$error = "";
$i = 0;
$images_count = sizeof($_FILES['userimage']['name']);
$user_id = $_SESSION['user_id'];

for ($i = 0; $i < $images_count; $i++)
{
    $image_name = $_FILES['userimage']['name'][$i];
    $image_type = $_FILES['userimage']['type'][$i];
    if(!empty($_FILES['userimage']['error'][$i]))
    {
        switch($_FILES['userimage']['error'][$i])
        {

            case '1':
                $error = 'the size of your image exceeds upload_max_filesize in php.ini ';
                break;
            case '2':
                $error = 'the size of your image exceeds MAX_FILE_SIZE in HTML form.';
                break;
            case '3':
                $error = 'only a part of the file was uploaded.';
                break;
            case '4':
                $error = 'the file was not uploaded ';
                break;
            case '6':
                $error = 'there is no such tmp directory';
                break;
            case '7':
                $error = 'writing file to disk error';
                break;
            case '8':
                $error = 'uploading the file was interrupted';
                break;
            case '999':
            default:
                $error = 'No error code available';
        }
    }
    elseif(!file_exists($_FILES['userimage']['tmp_name'][$i]) || $_FILES['userimage']['tmp_name'][$i] == 'none')
    {
        $error = 'No file was uploaded..';
    }
    else
    {
        if (file_exists(UPLOADPATH . $user_id) && file_exists(UPLOADPATH . $user_id . '/' . $image_name))
        {
            $error = $image_name . " already exists. ";
        }
        elseif(file_exists(UPLOADPATH . $user_id) && !file_exists(UPLOADPATH . $user_id . '/' . $image_name))
        {
            $target = UPLOADPATH . $user_id .'/'. $image_name;
            move_uploaded_file($_FILES['userimage']['tmp_name'][$i], $target);
            $query = "INSERT INTO images (image_name, image_type, upload_date, user_id) VALUES ('$image_name', '$image_type', NOW(), '$user_id')";
            mysqli_query($dbc, $query);
            echo "<p>Image(s) Successfully Uploaded" . "<br /></p>";
        }
        else
        {
            mkdir(UPLOADPATH . $user_id);
            $target = UPLOADPATH . $user_id .'/'. $image_name;
            move_uploaded_file($_FILES['userimage']['tmp_name'][$i], $target);
            $query = "INSERT INTO images (image_name, image_type, upload_date, user_id) VALUES ('$image_name', '$image_type', NOW(), '$user_id')";
            mysqli_query($dbc, $query);
            echo "<p>Image(s) Successfully Uploaded" . "<br /></p>";
        }
        //removing all the uploaded files
        @unlink($_FILES['userimage'][$i]);
    }

  echo				"<p>error: '" . $error . "',\n</p>";
}
