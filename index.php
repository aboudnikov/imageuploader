<?php
require_once('session.php');
$page_title = 'ImageUploader';
require_once('templates/header.php');
require_once('connectioninfo.php');
require_once('menu.php');
?>
<div id="site_content">
    <div class="sidebar_container">
        <div class="sidebar">
            <div class="sidebar_item">
                <h2>New Website</h2>
                <p>Welcome to our new website. Please have a look around, any feedback is much appreciated.</p>
            </div>
        </div>
        <div class="sidebar">
            <div class="sidebar_item">
                <h2>Latest Update</h2>
                <h3>March 2013</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque cursus tempor enim.</p>
            </div>
        </div>
        <div class="sidebar">
            <div class="sidebar_item">
                <h3>February 2013</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque cursus tempor enim.</p>
            </div>
        </div>
        <div class="sidebar">
            <div class="sidebar_item">
                <h3>January 2013</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque cursus tempor enim.</p>
            </div>
        </div>
        <div class="sidebar">
            <div class="sidebar_item">
                <h2>Contact</h2>
                <p>Phone: +375 (29)676-33-80</p>
                <p>Email: <a href="mailto:boudnikovandrew@gmail.com">boudnikovandrew@gmail.com</a></p>
            </div>
        </div>
    </div>

    <div class="slideshow">
        <ul class="slideshow">
            <li class="show"><img width="680" height="250" src="images/home_1.jpg" alt="&quot;Enter your caption here&quot;" /></li>
            <li><img width="680" height="250" src="images/home_2.jpg" alt="&quot;Enter your caption here&quot;" /></li>
        </ul>
    </div>

    <div id="content">
        <div class="content_item">
            <h1>Welcome To ImageUploader Wed Service</h1>
            <p>This is one of the best cloud services for uploading pictures. Try it for free now!</p>

            <div class="content_image">
                <img src="images/content_image1.jpg" alt="image1"/>
            </div>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque cursus tempor enim. Aliquam facilisis neque non nunc posuere eget volutpat metus tincidunt.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque cursus tempor enim.</p>
            <br style="clear:both"/>

            <div class="content_container">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque cursus tempor enim. Aliquam facilisis neque non nunc posuere eget volutpat metus tincidunt.</p>
                <div class="button_small">
                    <a href="#">Read more</a>
                </div>
            </div>
            <div class="content_container">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque cursus tempor enim. Aliquam facilisis neque non nunc posuere eget volutpat metus tincidunt.</p>
                <div class="button_small">
                    <a href="#">Read more</a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
require_once('templates/footer.php');
?>
