<?php
require_once('session.php');
require_once('connectioninfo.php');
require_once('dbconnect.php');
require_once('templates/header.php');
require_once('menu.php');
if (!isset($_SESSION['user_id']))
{ ?>
    <div id="site_content">
<?php
    echo '<p>Please <a href="login.php">log in</a> or <a href="signup.php">sing up</a> to access this page.</p>';
?>
    </div>
<?php
    exit();
}
?>


<div id="site_content">
    <div class="form_settings">
        <form id="uploadForm" action="doupload.php" method="post" enctype="multipart/form-data">
            <input name="MAX_FILE_SIZE" value="<?php echo MAXFILESIZE; ?>" type="hidden"/>
            <p>File: <input name="userimage[]" id="userimage" class="MultiFile" type="file"/</p>
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="submit" name="submit" value="Submit" /></p>
        </form>
    </div>
<img id="loading" src="images/loading.gif" style="display:none;"/>
<div id="uploadOutput"></div>
</div>

<?php
    require_once('templates/footer.php');
?>

<script type="text/javascript">
    $(document).ready(function()
    {

        $('.MultiFile').MultiFile(
            { //sets restrictions
                accept:'jpg|gif|png', max:15, STRING:
            {
                remove:'delete',
                file:'$file',
                selected:'Selected: $file',
                denied:'Wrong file type: $ext!',
                duplicate:'You have already chosen that file:\n$file!'
            }
            });


        $form = $('#uploadForm');
        $('#uploadForm').ajaxForm(
            {
                beforeSubmit: function(a,f,o)
                {
                    o.dataType = "html";
                    $('#uploadOutput').html('Submitting...');
                },
                success: function(data)
                {
                    var $out = $('#uploadOutput');
                    $out.html('');
                    if (typeof data == 'object' && data.nodeType)
                        data = elementToString(data.documentElement, true);
                    else if (typeof data == 'object')
                        data = objToString(data);
                    $out.append('<div><pre>'+ data +'</pre></div>');
                    $('#userimage_wrap_list').html('');
                }

            });

    });
</script>

