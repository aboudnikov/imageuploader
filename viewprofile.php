<?php
require_once('session.php');
$page_title = 'Your Profile';
require_once('templates/header.php');
require_once('connectioninfo.php');
require_once('dbconnect.php');

// Make sure the user is logged
if (!isset($_SESSION['user_id']))
{
    echo '<p class="login">Please <a href="login.php">log in</a> to access this page.</p>';
    exit();
}

// Getting user data from th DB
if (!isset($_GET['user_id']))
{
    $query = "SELECT username, email FROM users WHERE user_id = '" . $_SESSION['user_id'] . "'";
}
else
{
    $query = "SELECT username, email FROM mismatch_user WHERE user_id = '" . $_GET['user_id'] . "'";
}
$data = mysqli_query($dbc, $query);

if (mysqli_num_rows($data) == 1)
{
    // Displaying the user data
    $row = mysqli_fetch_array($data);

require_once('menu.php');
?>

<div id="site_content">
    <?php
        echo '<table>';
        if (!empty($row['username']))
        {
            echo '<tr><td class="contact">Username:</td><td>' . $row['username'] . '</td></tr>';
        }
        if (!empty($row['email']))
        {
            echo '<tr><td class="contact">Email:</td><td>' . $row['email'] . '</td></tr>';
        }
        echo '</table>';
    ?>

</div>

<?php
}
else
{
    echo '<p class="error">There was a problem accessing your profile.</p>';
}
mysqli_close($dbc);
require_once('templates/footer.php');
?>