<?php
require_once('session.php');
require_once('connectioninfo.php');
require_once('dbconnect.php');
$error_msg = "";

// Logging in the user if he is not logged in
if (!isset($_SESSION['user_id']))
{
    if (isset($_POST['submit']))
    {
        $user_email = mysqli_real_escape_string($dbc, trim($_POST['email']));
        $user_password = mysqli_real_escape_string($dbc, $_POST['password']);

        if (!empty($user_email) && !empty($user_password))
        {
            $query = "SELECT user_id, email FROM users WHERE email = '$user_email' AND password = SHA('$user_password')";
            $data = mysqli_query($dbc, $query);

            if (mysqli_num_rows($data) == 1)
            {
                // Setting the user ID and username session variables and cookies, and redirecting to the upload images page
                $row = mysqli_fetch_array($data);
                $_SESSION['user_id'] = $row['user_id'];
                $_SESSION['email'] = $row['email'];
                setcookie('user_id', $row['user_id'], time() + (60 * 60 * 24 * 30));
                setcookie('email', $row['email'], time() + (60 * 60 * 24 * 30));
                $home_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/uploadimages.php';
                header('Location: ' . $home_url);

            }
            else
            {
                // The email/password are incorrect so set an error message
                $error_msg = 'Sorry, you must enter a valid email and password to log in.';
            }
        }
        else
        {
            // The email/password weren't entered so set an error message
            $error_msg = 'Sorry, you must enter your email and password to log in.';
        }
    }
}
$page_title = 'Log In';
require_once('templates/header.php');
require_once('menu.php');
// If the session var is empty, show any error message and the log-in form; otherwise confirm the log-in
if (empty($_SESSION['user_id']))
{
    echo '<p>' . $error_msg . '</p>'; ?>

<div id="site_content">
    <div class="form_settings">
        <div class="error_box"></div>
        <form id="login_form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                <h2>Log In</h2>
                <p><span>Email</span><input class="contact" type="text" name="email" value="<?php if (!empty($user_email)) echo $user_email; ?>" /></p>
                <p><span>Password</span><input class="contact" type="password" name="password" /></p>
                <p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="submit" name="submit" value="Log In" /></p>
        </form>
    </div>
</div>
<?php
}
else
{
    echo ('<p>You are logged in as ' . $_SESSION['email'] . '.</p>');
    echo ('<p><a href="logout.php">Logout</a></p>');
}
require_once('templates/footer.php');
?>

<script type="text/javascript">
    new FormValidator('login_form', [{
        name: 'email',
        rules: 'required'
    }, {
        name: 'password',
        rules: 'required'
    }], function (errors, evt)
    {
        var SELECTOR_ERRORS = $('.error_box');
        if (errors.length > 0) {
            SELECTOR_ERRORS.empty();
            for (var i = 0, errorLength = errors.length; i < errorLength; i++) {
                SELECTOR_ERRORS.append('<p>' + errors[i].message + '</p>');
            }
            SELECTOR_ERRORS.fadeIn(200);
            if (evt && evt.preventDefault) {
                evt.preventDefault();
            } else if (event) {
                event.returnValue = false;
            }
        } else {
            SELECTOR_ERRORS.css({ display: 'none' });
        }
    });
</script>

