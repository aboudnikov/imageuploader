<?php
require_once('session.php');
require_once('connectioninfo.php');
$page_title = 'Image View';
require_once('templates/header.php');
require_once('dbconnect.php');

$query = "SELECT * FROM images where image_id = '" . $_GET['id'] . "'";
$data = mysqli_query($dbc, $query);
$row = mysqli_fetch_array($data);
$path = UPLOADPATH . $_SESSION['user_id'] . '/' . $row['image_name'];
require_once('menu.php');
?>
<div id="site_content">
    <div class="full_image">
        <?php
            if($_SESSION['user_id'] == $row['user_id'])
            {
                echo "<img src='" . $path . "'>";
            }
            else
            {
                echo "The image doesn't exist!";
            }
        ?>
    </div>
</div>

