<?php
require_once('connectioninfo.php');
require_once('session.php');
require_once('dbconnect.php');
$page_title = 'Image Removal';
require_once('templates/header.php');

if (isset($_GET['image_id']) && isset($_GET['image_name']) && isset($_GET['image_type']) && isset($_GET['upload_date']) && isset($_GET['user_id']))
{
    // Receiving data about the image from GET
    $image_id = $_GET['image_id'];
    $image_name = $_GET['image_name'];
    $image_type = $_GET['image_type'];
    $upload_date = $_GET['upload_date'];
    $user_id = $_GET['user_id'];
}
else if (isset($_POST['image_id']) && isset($_POST['image_name']))
{
    // When we confirm picture removal it sends POST request and we get image data from POST
    $image_id = $_POST['image_id'];
    $image_name = $_POST['image_name'];
}
else
{
    echo '<p>Sorry, no high score was specified for removal.</p>';
}

if (isset($_POST['submit']))
{
    if ($_POST['confirm'] == 'Yes') {
        // Deleting the image file from the server
        @unlink(UPLOADPATH . $_SESSION['user_id'] . '/' . $image_name);

        // Deleting image data from the database
        $query = "DELETE FROM images WHERE image_id = $image_id LIMIT 1";
        mysqli_query($dbc, $query);
        mysqli_close($dbc);
        echo '<p>The image ' . $image_name . ' was successfully removed.';
    }
    else
    {
        echo '<p>The image was not removed.</p>';
    }
}
else if (isset($image_id) && isset($upload_date) && isset($image_type) && isset($user_id))
{ ?>
<div id="site_content">
    <div class="form_settings">
<?php
    echo '<p>Are you sure you want to delete the following high score?</p>';
    echo '<p><strong>Name: </strong>' . $image_name . '<br /><strong>Date: </strong>' . $upload_date .
        '<br /><strong>Image Type: </strong>' . $image_type . '</p>';
    echo '<form method="post" action="deleteimage.php">';
    echo '<p><input type="radio" name="confirm" value="Yes" /> Yes </p>';
    echo '<p><input type="radio" name="confirm" value="No" checked="checked" /> No <br /></p>';
    echo '<p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="submit" name="submit" value="Submit" /></p>';
    echo '<input type="hidden" name="image_id" value="' . $image_id . '" />';
    echo '<input type="hidden" name="image_name" value="' . $image_name . '" />';
    echo '</form>';
}
echo '<p><a href="showimages.php">&lt;&lt; Back to images</a></p>';
?>
    </div>
</div>
<?php
require_once('templates/footer.php');
?>
