<?php
require_once('session.php');
$page_title = 'Edit Profile';
require_once('templates/header.php');
require_once('connectioninfo.php');
require_once('dbconnect.php');

if (!isset($_SESSION['user_id']))
{
    echo '<p>Please <a href="login.php">log in</a> to access this page.</p>';
    exit();
}

if (isset($_POST['submit']))
{
    $username = mysqli_real_escape_string($dbc, trim($_POST['username']));
    $email = mysqli_real_escape_string($dbc, trim($_POST['email']));
    if (!empty($username) && !empty($email))
    {
        $query = "UPDATE users SET username = '$username', email = '$email' WHERE user_id = '" . $_SESSION['user_id'] . "'";
        mysqli_query($dbc, $query);
        echo '<p>Your profile has been successfully updated. Would you like to <a href="viewprofile.php">view your profile</a>?</p>';
        echo '<a href="index.php">Main Page</a>';
        mysqli_close($dbc);
        exit();
    }
    else
    {
        echo '<p>You must enter all of the profile data (the picture is optional).</p>';
    }
}
else
{
    $query = "SELECT username, email FROM users WHERE user_id = '" . $_SESSION['user_id'] . "'";
    $data = mysqli_query($dbc, $query);
    $row = mysqli_fetch_array($data);

    if ($row != NULL)
    {
        $username = $row['username'];
        $email = $row['email'];
    }
    else
    {
        echo '<p>There was a problem accessing your profile.</p>';
    }
}
mysqli_close($dbc);
require_once('menu.php');
?>

<div id="site_content">
    <div class="form_settings">
        <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
              <h2>Personal Information</h2>
                <p><span>Username: </span><input class="contact" type="text" id="username" name="username" value="<?php if (!empty($username)) echo $username; ?>" /></p>
                <p><span>Email: </span><input type="text" id="email" name="email" value="<?php if (!empty($email)) echo $email; ?>" /></p>
                <p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="submit" name="submit" value="Save Profile" /></p>
        </form>
    </div>
</div>

<?php
    require_once('templates/footer.php');
?>