<?php
require_once('session.php');
require_once('connectioninfo.php');
require_once('dbconnect.php');
$page_title = 'Sign Up';

if (isset($_POST['submit']))
{
    $username = mysqli_real_escape_string($dbc, trim($_POST['username']));
    $email = mysqli_real_escape_string($dbc, trim($_POST['email']));
    $password1 = mysqli_real_escape_string($dbc, $_POST['password1']);
    $password2 = mysqli_real_escape_string($dbc, $_POST['password2']);

    if (!empty($username) && !empty($email) && !empty($password1) && !empty($password2) && ($password1 == $password2))
    {
        $query = "SELECT * FROM users WHERE email = '$email'";
        $data = mysqli_query($dbc, $query);
        if (mysqli_num_rows($data) == 0)
        {
            $query = "INSERT INTO users (username, email, password, join_date) VALUES ('$username', '$email', SHA('$password1'), NOW())";
            mysqli_query($dbc, $query);
            $user_id = mysqli_insert_id($dbc);
            $_SESSION['user_id'] = $user_id;
            $_SESSION['email'] = $email;
            setcookie('user_id', $user_id, time() + (60 * 60 * 24 * 30));
            setcookie('email', $email, time() + (60 * 60 * 24 * 30));
            mysqli_close($dbc);
            $home_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/uploadimages.php';
            header('Location: ' . $home_url);
            exit();
        }
    }
}
mysqli_close($dbc);
require_once('templates/header.php');
?>


<?php
require_once('menu.php');
?>

<div id="site_content">
    <div class="form_settings">
        <p>Please enter your username and password to sign up to ImageUploader.</p>
        <div class="error_box"></div>
        <form id="registration_form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
              <h2>Registration Info</h2>
              <p><span>Username</span><input type="text" id="username" name="username" value="<?php if (!empty($username)) echo $username; ?>" /></p>
              <p><span>Email</span><input type="text" id="email" name="email" value="<?php if (!empty($email)) echo $email; ?>" /></p>
              <p><span>Password</span><input type="password" id="password1" name="password1" /></p>
              <p><span>Password (retype):</span><input type="password" id="password2" name="password2" /></p>
              <p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="submit" name="submit" value="Sign Up" /></p>
        </form>
    </div>
</div>

<?php
    require_once('templates/footer.php');
?>

<script type="text/javascript">
    new FormValidator('registration_form', [{
        name: 'email',
        rules: 'required|valid_email'
    }, {
        name: 'password1',
        rules: 'required|min_length[7]'
    }, {
        name: 'password2',
        display: 'password confirmation',
        rules: 'required|matches[password1]'
    }], function (errors, evt)
    {
        var SELECTOR_ERRORS = $('.error_box');
        if (errors.length > 0) {
            SELECTOR_ERRORS.empty();
            for (var i = 0, errorLength = errors.length; i < errorLength; i++) {
                SELECTOR_ERRORS.append('<p>' + errors[i].message + '</p>');
            }
            SELECTOR_ERRORS.fadeIn(200);
            if (evt && evt.preventDefault) {
                evt.preventDefault();
            } else if (event) {
                event.returnValue = false;
            }
        } else {
            SELECTOR_ERRORS.css({ display: 'none' });
        }
    });
</script>