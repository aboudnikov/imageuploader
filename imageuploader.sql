-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Ноя 18 2013 г., 11:23
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `imageuploader`
--

-- --------------------------------------------------------

--
-- Структура таблицы `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `image_name` text,
  `image_type` text,
  `upload_date` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=163 ;

--
-- Дамп данных таблицы `images`
--

INSERT INTO `images` (`image_id`, `image_name`, `image_type`, `upload_date`, `user_id`) VALUES
(135, 'image1.jpg', 'image/jpeg', '2013-11-18 10:18:54', 32),
(136, 'iamge2.jpg', 'image/jpeg', '2013-11-18 10:18:54', 32),
(137, 'image3.jpg', 'image/jpeg', '2013-11-18 10:18:54', 32),
(138, 'image4.jpg', 'image/jpeg', '2013-11-18 10:18:54', 32),
(139, 'image5.jpg', 'image/jpeg', '2013-11-18 10:18:54', 32),
(140, 'image6.jpg', 'image/jpeg', '2013-11-18 10:18:54', 32),
(141, 'image7.jpg', 'image/jpeg', '2013-11-18 10:18:54', 32),
(142, 'image8.jpg', 'image/jpeg', '2013-11-18 10:18:54', 32),
(143, 'image9.jpg', 'image/jpeg', '2013-11-18 10:18:54', 32),
(144, 'image10.jpg', 'image/jpeg', '2013-11-18 10:18:54', 32),
(145, 'image11.jpg', 'image/jpeg', '2013-11-18 10:18:54', 32),
(146, 'image14.jpg', 'image/jpeg', '2013-11-18 10:18:54', 32),
(147, 'image15.jpg', 'image/jpeg', '2013-11-18 10:18:54', 32),
(148, 'image16.jpg', 'image/jpeg', '2013-11-18 10:20:52', 32),
(149, 'image17.jpg', 'image/jpeg', '2013-11-18 10:20:52', 32),
(150, 'image18.jpg', 'image/jpeg', '2013-11-18 10:20:52', 32),
(151, 'image19.jpg', 'image/jpeg', '2013-11-18 10:20:52', 32),
(152, 'image20.jpg', 'image/jpeg', '2013-11-18 10:20:52', 32),
(153, 'image21.jpg', 'image/jpeg', '2013-11-18 10:20:52', 32),
(154, 'image22.jpg', 'image/jpeg', '2013-11-18 10:20:52', 32),
(155, 'image23.jpg', 'image/jpeg', '2013-11-18 10:20:52', 32),
(156, 'image24.jpg', 'image/jpeg', '2013-11-18 10:20:52', 32),
(157, 'image25.jpg', 'image/jpeg', '2013-11-18 10:20:52', 32),
(158, 'image26.jpg', 'image/jpeg', '2013-11-18 10:20:52', 32),
(159, 'image27.jpg', 'image/jpeg', '2013-11-18 10:20:52', 32),
(160, 'image28.jpg', 'image/jpeg', '2013-11-18 10:20:52', 32),
(161, 'image29.jpg', 'image/jpeg', '2013-11-18 10:20:52', 32),
(162, 'image30.jpg', 'image/jpeg', '2013-11-18 10:20:52', 32);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `join_date` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `email`, `join_date`) VALUES
(32, 'Andrew', '895ffa7e279482b0543e7a61d7803d7a6b1fe66e', 'boudnikovandrew@gmail.com', '2013-11-18 10:17:11');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
