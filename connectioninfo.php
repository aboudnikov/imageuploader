<?php
// DB connection constants
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASSWORD', '');
define('DB_NAME', 'imageuploader');
define('MAXFILESIZE', 1500000);
define('UPLOADPATH', 'media/images/');
?>